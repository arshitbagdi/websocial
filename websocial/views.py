from django.shortcuts import render, redirect
from django.views.generic import TemplateView

class IndexView(TemplateView):
    template_name='index.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('chat:chat_list')
        else:
            return super(IndexView, self).dispatch(request, *args, **kwargs)
