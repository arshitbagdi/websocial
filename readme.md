
To setup the application you need to install docker and docker compose

    install docker (refer to this link - https://docs.docker.com/engine/install/)
    install docker-compose (refer to this link - https://docs.docker.com/compose/install/)

Run following commands to setup the project - 

    docker-compose build
    docker-compose run --rm web python manage.py migrate
    docker-compose run --rm web python manage.py createsuperuser

Run following command to run the project -

    docker-compose up


now go to http://localhost:8000/