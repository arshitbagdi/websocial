from django.shortcuts import render, redirect
from accounts import forms
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.db.models import Q, Max, BooleanField, Value, Count
from django.http import JsonResponse
from itertools import chain
from operator import itemgetter

from chat.models import PublicRoom, PrivateRoom
# Create your views here.

def sign_up(request):

    if request.user.is_authenticated:
        return redirect('chat:chat_list')

    if request.method == "POST":
        user_form = forms.UserForm(request.POST)

        if user_form.is_valid():
            user = user_form.save()
            new_user = authenticate(username=user_form.cleaned_data['username'],password=user_form.cleaned_data['password1'],)
            login(request, new_user)
            return redirect('chat:chat_list')

        else:
            user_form = user_form

    else :
        user_form = forms.UserForm()

    return render(request,'accounts/signup.html',{'user_form':user_form})


def search_user(request):
    search_query = request.GET.get('search_query')
    users_found = False
    if search_query is None or search_query=="":
        users_found = True
        users = list(User.objects.all().exclude(id=request.user.id).values('id','first_name','last_name'))
        groups = PublicRoom.objects.filter(users=request.user.id).annotate(is_group=Value(True,output_field=BooleanField())).values('title','is_group','id','last_active_at')
        for group in groups:
            last_message = PublicRoom.objects.filter(id=group["id"]).first().public_room.last().content
            group["last_message"] = last_message
        private_chats = PrivateRoom.objects.filter(Q(user_one=request.user.id)|Q(user_two=request.user.id)).annotate(is_group=Value(False,output_field=BooleanField()),message_count=Count('private_room')).filter(message_count__gt=0).values('title' ,'id', 'is_group','last_active_at' ,'user_one__id','user_one__first_name','user_one__last_name','user_two__id','user_two__first_name','user_two__last_name')
        for private_chat in private_chats:
            last_message = PrivateRoom.objects.filter(id=private_chat["id"]).first().private_room.last().content
            private_chat["last_message"] = last_message
        chat_list = list(chain(groups, private_chats))
        chat_list.sort(key=itemgetter('last_active_at'),reverse=True)
        return JsonResponse({"users_found":users_found,"chat_list":chat_list})

    else:
        users = User.objects.filter((Q(first_name__istartswith=search_query)|Q(last_name__istartswith=search_query))&~Q(id=request.user.id)).values('id','first_name','last_name')
        private_chats = PrivateRoom.objects.filter(
            (Q(user_one=request.user.id)&(Q(user_two__first_name__istartswith=search_query)|Q(user_two__last_name__istartswith=search_query)))|(Q(user_two=request.user.id)&(Q(user_one__first_name__istartswith=search_query)|Q(user_one__last_name__istartswith=search_query)))
        ).annotate(
            message_count=Count('private_room')
        ).filter(message_count__gt=0)
        private_chats = private_chats.values('title' ,'id', 'last_active_at' ,'user_one__id','user_one__first_name','user_one__last_name','user_two__id','user_two__first_name','user_two__last_name')
        for private_chat in private_chats:
            last_message = PrivateRoom.objects.filter(id=private_chat["id"]).first().private_room.last().content
            private_chat["last_message"] = last_message
        private_chats_ids = private_chats.values_list('user_one__id','user_two__id')
        private_chats_ids = [item for t in private_chats_ids for item in t]
        users = users.exclude(id__in=private_chats_ids)
        groups = PublicRoom.objects.filter(users=request.user.id,title__istartswith=search_query).annotate(is_group=Value(True,output_field=BooleanField())).values('title','is_group','id','last_active_at')
        for group in groups:
            last_message = PublicRoom.objects.filter(id=group["id"]).first().public_room.last().content
            group["last_message"] = last_message
        chat_list = list(chain(private_chats, groups, users))
        if users.count()>0 or groups.count()>0 or private_chats.count()>0:
            users_found = True
            users = list(chat_list)
        else:
            users = None

        return JsonResponse({"users_found":users_found,"users":users})

def get_users(request):
    users = list(User.objects.all().exclude(id=request.user.id).order_by('first_name').values('id','first_name','last_name'))
    return JsonResponse({"users":users})