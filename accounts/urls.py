from django.contrib import admin
from django.urls import path
from .views import sign_up, search_user, get_users

app_name = 'accounts'

urlpatterns = [
    path('signup/', sign_up, name='signup'),
    path('search_user/', search_user, name='search_user'),
    path('get_user/', get_users, name='users'),
]