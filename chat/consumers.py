import json
from channels.generic.websocket import AsyncWebsocketConsumer
from .models import PrivateMessage, PrivateRoom, PublicRoom, PublicMessage
from channels.db import database_sync_to_async
from django.contrib.auth.models import User
from django.db.models import Q
from django.core.serializers.json import DjangoJSONEncoder

class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.user_or_group_id = self.scope['url_route']['kwargs']['room_name']
        self.is_group = self.scope['url_route']['kwargs']['is_group']
        self.is_group = False if self.is_group == "false" else True
        self.room_name = await get_room_name(self.scope,self.user_or_group_id,self.is_group)
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        user = text_data_json['user']
        command = text_data_json['command']

        if command == "send_message":
            message = text_data_json['message']
            await save_private_or_group_messages(message,user,self.room_name, self.is_group, self.user_or_group_id)
            # Send message to room group
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'chat_message',
                    'message': message,
                    'user': user,
                }
            )
        elif command == "get_private_or_group_chat_message":
            messages = await get_private_or_group_messages(self.scope , self.user_or_group_id, self.room_name, self.is_group,)
            await self.send_private_chat_messages(messages)

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']
        user = event['user']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message,
            'user': user,
        }))

    async def send_private_chat_messages(self, messages):

        await self.send(text_data=json.dumps(
            {
                "private_chat_messages": "private_chat_messages",
                "messages": messages,
            },
        ))

@database_sync_to_async
def get_room_name(scope,user_or_group_id,is_group):
    if is_group:
        room_name = user_or_group_id+'-group'
    else:
        user_one = scope["user"]
        user_two = user_or_group_id
        room = PrivateRoom.objects.filter(Q(user_one=user_one,user_two=user_two)|Q(user_one=user_two,user_two=user_one))

        if room.count() == 0:
            user = User.objects.get(id=user_two)
            room = PrivateRoom.objects.create(title=str(user_one.id)+user_two,user_one=user_one,user_two=user)
        else:
            room = room.first()

        room_name=room.title
    return room_name


@database_sync_to_async
def save_private_or_group_messages(content, user, room_name, is_group, user_or_group_id):
    if is_group:
        room = PublicRoom.objects.filter(id=user_or_group_id).first()
        user = User.objects.filter(id=user).first()
        PublicMessage.objects.create(content=content,user=user,room=room)
        room.save()

    else:
        room = PrivateRoom.objects.filter(title=room_name).first()
        user = User.objects.filter(id=user).first()
        PrivateMessage.objects.create(content=content,user=user,room=room)
        room.save()


@database_sync_to_async
def get_private_or_group_messages(scope, user_or_group_id, room_name, is_group):
    if is_group:
        room = PublicRoom.objects.filter(id=user_or_group_id).first()
        messages = PublicMessage.objects.filter(room=room).order_by('date').values('content','user','date')
    else:
        user_one = scope["user"]
        user_two = User.objects.filter(id=user_or_group_id).first()
        room = PrivateRoom.objects.filter(title=room_name).first()
        messages = PrivateMessage.objects.filter(Q(room=room)&(Q(user=user_one)|Q(user=user_two))).order_by('date').values('content','user','date','is_seen')

    messages = json.dumps(
        list(messages),
        sort_keys=True,
        indent=1,
        cls=DjangoJSONEncoder
    )
    return messages