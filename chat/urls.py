from django.contrib import admin
from django.urls import path
from .views import room, chat_list, create_group

app_name = 'chat'

urlpatterns = [
    path('room/<str:room_name>/', room, name='room'),
    path('list/', chat_list, name='chat_list'),
    path('create_group/', create_group, name='create_group'),
]