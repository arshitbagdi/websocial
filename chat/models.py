from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class PublicRoom(models.Model):
	title = models.CharField(max_length=256)
	users = models.ManyToManyField(User,blank=True)
	last_active_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.title


class PublicMessage(models.Model):
	content = models.CharField(max_length=256,blank=False)
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	seen_by = models.ManyToManyField(User,related_name="seen_by_users")
	room = models.ForeignKey(PublicRoom,related_name="public_room",on_delete=models.CASCADE)
	date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return "%s - %s"%(self.user,self.room)

class PrivateRoom(models.Model):
	title = models.CharField(max_length=256)
	user_one = models.ForeignKey(User,blank=True,on_delete=models.CASCADE,related_name='user_one')
	user_two = models.ForeignKey(User,blank=True,on_delete=models.CASCADE,related_name='user_two')
	last_active_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return "%s - %s"%(self.user_one,self.user_two)

class PrivateMessage(models.Model):
	content = models.CharField(max_length=256,blank=False)
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	is_seen = models.BooleanField(default=False)
	room = models.ForeignKey(PrivateRoom,related_name="private_room",on_delete=models.CASCADE)
	date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return "%s - %s"%(self.user,self.room)