import json
from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.db.models import Max, Q, BooleanField, Value, Count
from itertools import chain

from .models import PrivateRoom, PublicRoom
from .forms import PublicRoomForm
# Create your views here.


def room(request, room_name):
    room = PrivateRoom.objects.filter(user_one=request.user,user_two=room_name)
    users = User.objects.all().exclude(id=request.user.id)

    if room.count() == 0:
        room = PrivateRoom.objects.filter(user_one=room_name,user_two=request.user)
        if room.count() == 0:
            user = User.objects.get(id=room_name)
            room = PrivateRoom.objects.create(title=str(request.user.id)+room_name,user_one=user,user_two=request.user)
        else:
            room = room.first()
    else:
        room = room.first()

    room_name=room.title
    return render(request, 'chat/chat_room_advanced.html', {
        'room_name': room_name,
        'users': users
    })


@login_required(login_url='/login')
def chat_list(request):
    users = User.objects.all().exclude(id=request.user.id)
    groups = PublicRoom.objects.filter(users=request.user.id).annotate(is_group=Value(True,output_field=BooleanField()))
    private_chats = PrivateRoom.objects.filter(Q(user_one=request.user.id)|Q(user_two=request.user.id)).annotate(is_group=Value(False,output_field=BooleanField()),message_count=Count('private_room')).filter(message_count__gt=0)
    chat_list = sorted(chain(groups, private_chats),key=lambda instance: instance.last_active_at, reverse=True)

    return render(request, 'chat/chat_room_advanced.html', {
        'chat_list': chat_list
    })

def create_group(request):

    if request.method == "POST":
        form = PublicRoomForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("chat:chat_list")
        else:
            form = form
    else:
        form = PublicRoomForm()

    return render(request,"chat/create_group.html",{"form":form})
    # return HttpResponse({"form":form})