from django import forms
from .models import PublicRoom

class PublicRoomForm(forms.ModelForm):
    class Meta:
        model = PublicRoom
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['users'].required = True
