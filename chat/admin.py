from django.contrib import admin
from .models import PublicMessage, PublicRoom, PrivateMessage, PrivateRoom
# Register your models here.


class PublicMessageAdmin(admin.ModelAdmin):
    list_display = ['user','room','date']


class PublicRoomAdmin(admin.ModelAdmin):
    list_display = ['title']

class PrivateMessageAdmin(admin.ModelAdmin):
    list_display = ['user','room','date']


class PrivateRoomAdmin(admin.ModelAdmin):
    list_display = ['title','user_one','user_two']


admin.site.register(PublicMessage,PublicMessageAdmin)
admin.site.register(PublicRoom,PublicRoomAdmin)
admin.site.register(PrivateMessage,PrivateMessageAdmin)
admin.site.register(PrivateRoom,PrivateRoomAdmin)