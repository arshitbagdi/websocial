from django.urls import re_path

from . import consumers

websocket_urlpatterns = [
    re_path(r'ws/chat/(?P<room_name>\w+)/(?P<is_group>\w+)/$', consumers.ChatConsumer.as_asgi()),
]